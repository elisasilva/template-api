const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

let stan = null;
let mongo = null;
let sec = null;

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));
  stan = stanConn;
  mongo = mongoClient;
  sec = secret;

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  // User Creation
  api.post("/users", handleUserCreation);

  // User Deletion
  api.delete("/users/:uuid", handleUserDeletion);

  return api;
};

/**
 * Create user.
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
async function handleUserCreation(req, res) {
  // validate body
  let [valid, msg] = validate(req.body, userCreationDescription)
  if(!valid) {
    res.status(400);
    res.send(msg);
    return;
  }

  // check passowrd
  if(req.body.password !== req.body.passwordConfirmation) {
    res.status(422);
    res.send({
      error: "Password confirmation did not match"
    });
    return;
  }

  // check email
  let collection = mongo.db("user").collection("user")

  let found = await collection.findOne({
    email: req.body.email
  })

  if(found != undefined) {
    res.status(403);
    res.send({
      error: "Email already in use"
    });
    return;
  }

  let userUUID = uuid();

  /*await collection.insertOne({
      id: userUUID,
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
  })*/

  stan.publish('users', JSON.stringify({
    eventType: "UserCreated",
    entityId: userUUID,
    entityAggregate: {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    },
  }));

  res.status(201);
  res.send({user: {
    id: userUUID,
    name: req.body.name,
    email: req.body.email,
  }});
}

const userCreationDescription = {
  name: {
    _: "requiredField",
    type: "nonEmptyString",
  },
  email: {
    _: "requiredField",
    type: "email",
  },
  password: {
    _: "requiredField",
    type: "password",
  },
  passwordConfirmation: {
    _: "requiredField",
    type: "password",
  }
}

/**
 * Delete user.
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
async function handleUserDeletion(req, res) {
  let auth = req.get("Authentication")
  if(!/^Bearer /.test(auth)) {
    res.status(401);
    res.send({
      error: "Access Token not found"
    });
    return;
  }

  if(jwt.verify(auth.slice(7), sec).id !== req.params.uuid) {
    res.status(403);
    res.send({
      error: "Access Token did not match User ID"
    });
    return;
  }

  // delete from db
  let collection = mongo.db("user").collection("user")

  let query = await collection.findOne({
    id: req.params.uuid
  })

  if(query != undefined) {
    await collection.deleteOne({
      id: req.params.uuid
    })
  }

  stan.publish('users', JSON.stringify({
    eventType: "UserDeleted",
    entityId: req.params.uuid,
    entityAggregate: {}
  }));

  res.status(200);
  res.send({id: req.params.uuid});
}

/**
 * Validate an object based of a description
 * @param {*} value 
 * @param {*} description 
 */
function validate(value, description) {
  for(key of Object.keys(description)) {
    if(description[key]["_"] == "requiredField" && value[key] == undefined) {
      return [false, {
        error: `Request body had missing field ${key}`
      }];
    }
  }

  for(key of Object.keys(description)) {
    if(!match(value[key], description[key]["type"])[0]) {
      return [false, {
        error: `Request body had malformed field ${key}`
      }];
    }
  }

  return [true, {}];
}

/**
 * Returns wheter a value matchs a rule
 * @param {*} value 
 * @param {*} ruleName 
 */
function match(value, ruleName) {
  if(matchRules[ruleName] == undefined)
    return [false, {
      error: `Rule ${ruleName} does not exist`
    }];
  return matchRules[ruleName](value);
}

let matchRules = {
  nonEmptyString: (value) => {
    if(typeof value === 'string' && value.length > 0)
      return [true, {}];
    return [false, {}];
  },
  password: (value) => {
    if(typeof value === 'string' && /^[A-Za-z0-9]{8,32}$/.test(value))
      return [true, {}];
    return [false, {}];
  },
  email: (value) => {
    // src: https://www.emailregex.com/
    if(typeof value === 'string' && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value))
      return [true, {}];
    return [false, {}];
  },
}